import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd
from PIL import Image

def label_bounding_box(path='./test/', file = '1.csv'):
    annotation_root_dir = path + 'annotation/'
    imageName = pd.read_csv(annotation_root_dir + file).columns.values[0]
    annotationDataFrame = np.asarray(pd.read_csv(annotation_root_dir + file, skiprows=1))
    
    annotationDataFrame_labeled = pd.DataFrame(annotationDataFrame, columns = ['x', 'y'])
    xs = annotationDataFrame_labeled['x']
    ys = annotationDataFrame_labeled['y']
    
    #now seperate the two eyes
    shift1=30
    shift2 = 60
    leftx = np.min(xs[133:152]) -shift1
    lefty = np.min(ys[133:152]) -shift1
    leftw = np.max(xs[133:152]) - np.min(xs[133:152]) +shift2
    lefth = np.max(ys[133:152]) - np.min(ys[133:152]) +shift2
    rightx = np.min(xs[113:133]) -shift1
    righty = np.min(ys[113:133]) -shift1
    rightw = np.max(xs[113:133]) - np.min(xs[113:133]) +shift2
    righth = np.max(ys[113:133]) - np.min(ys[113:133]) +shift2
    boundingBoxleft = [leftx, lefty, leftw, lefth]
    boundingBoxRight = [rightx, righty, rightw, righth]
    boundingBoxes = np.array([boundingBoxleft, boundingBoxRight]).round()
    return boundingBoxes