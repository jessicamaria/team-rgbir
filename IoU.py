import numpy as np

def IoU(pred, target):
    l = ['left','right']
    U = np.zeros(2)
    for i in range(2):
        s1 = pred[i,2]*pred[i,3]
        s2 = target[i,2]*target[i,3]
        min_x = max(pred[i,0], target[i,0])
        min_y = max(pred[i,1], target[i,1])
        max_x = min(pred[i,0]+pred[i,2], target[i,0]+target[i,2])
        max_y = min(pred[i,1]+pred[i,3], target[i,1]+target[i,3])
        inter_s = (max_x-min_x)*(max_y-min_y)
        U[i] = inter_s / (s1+s2-inter_s)
        print('The IoU for the %s eye is %f.'%(l[i], U[i]))
        