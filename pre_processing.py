import os
import numpy as np
import torch
from torch import nn
from torch.nn import functional as F
import torch.utils.data as td
import torchvision as tv
import pandas as pd
from PIL import Image
from matplotlib import pyplot as plt
import matplotlib.patches as patches

annotation_root_dir = './train/annotation/'
output_root_dir = './train/'
img_root_dir = './helen/images/'
for root,dirs,files in os.walk(annotation_root_dir):
    for file in files:
        if file.endswith(".csv"):
            imageName = pd.read_csv(annotation_root_dir + "/" + file).columns.values[0]
            img_path = os.path.join(img_root_dir, imageName+'.jpg')
            I = Image.open(img_path).convert('RGB')
            org_size = I.size
            scale = max(I.size) / 448
            I.save(output_root_dir + 'data/' + imageName + ".jpg")
            
            annotationDataFrame = np.asarray(pd.read_csv(annotation_root_dir + "/" + file, skiprows=1))  
            annotationDataFrame_labeled = pd.DataFrame(annotationDataFrame, columns = ['x', 'y'])
            xs = annotationDataFrame_labeled['x']
            ys = annotationDataFrame_labeled['y']
            
            shift2=60
            left_cx = round((np.max(xs[133:152]) + np.min(xs[133:152]))//2 / scale)
            left_cy = round((np.max(ys[133:152]) + np.min(ys[133:152]))//2 / scale)
            leftw = round((np.max(xs[133:152]) - np.min(xs[133:152]) + shift2) / scale)
            lefth = round((np.max(ys[133:152]) - np.min(ys[133:152]) + shift2) / scale)
            
            right_cx = round((np.max(xs[113:133]) + np.min(xs[113:133]))//2 / scale)
            right_cy = round((np.max(ys[113:133]) + np.min(ys[113:133]))//2 / scale)
            rightw = round((np.max(xs[113:133]) - np.min(xs[113:133]) +shift2) / scale)
            righth = round((np.max(ys[113:133]) - np.min(ys[113:133]) +shift2) / scale)
            #left = [left_cx, left_center_y, leftw, lefth]
            #right = [right_cx, right_center_y, rightw, righth]
            #label_paramter = np.array([left, right])
            
            
            label = np.zeros((5,7,7))
            label[0, left_cx // 64, left_cy // 64] = 64
            label[1:, left_cx // 64, left_cy // 64] = np.array([left_cx%64, left_cy%64, leftw, lefth])
            label[0, right_cx // 64, right_cy // 64] = 64
            label[1:, right_cx // 64, right_cy // 64] = np.array([right_cx%64, right_cy%64, rightw, righth])
            label = label / 64
            output_path = os.path.join(output_root_dir+'label/', imageName+'.jpg.npy')
            np.save(output_path, label.astype(float))