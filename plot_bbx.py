import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches

def plot_bbx(image, bbx, ax=plt):
    h = ax.imshow(image)
    ax.axis('off')
    
    rect = patches.Rectangle((bbx[0,0],bbx[0,1]),bbx[0,2],bbx[0,3],linewidth=2,edgecolor='r',facecolor='none')
    # Add the patch to the Axes
    h = ax.add_patch(rect)
    
    rect = patches.Rectangle((bbx[1,0],bbx[1,1]),bbx[1,2],bbx[1,3],linewidth=2,edgecolor='r',facecolor='none')
    # Add the patch to the Axes
    h = ax.add_patch(rect)
    
    return h