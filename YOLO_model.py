import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import numpy as np


class YOLO_Net(nn.Module):
    # The YOLO_Net get a 448*448*3 input, and generate a 7*7*5 output
    
    def __init__(self):
        super(YOLO_Net, self).__init__()
        # Convolutional kernels
        self.conv1 = nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3)
        self.conv2 = nn.Conv2d(64, 192, kernel_size=3, padding=1)
        
        self.conv3 = nn.ModuleList([nn.Conv2d(192, 128, kernel_size=1, padding=0),
                                   nn.Conv2d(128, 256, kernel_size=3, padding=1),
                                   nn.Conv2d(256, 256, kernel_size=1, padding=0),
                                   nn.Conv2d(256, 512, kernel_size=3, padding=1)])
        
        self.conv4 = nn.ModuleList()
        for i in range (4):
            self.conv4.append(nn.Conv2d(512, 256, kernel_size=1, padding=0))
            self.conv4.append(nn.Conv2d(256, 512, kernel_size=3, padding=1))
        self.conv4.append(nn.Conv2d(512, 512, kernel_size=1, padding=0))
        self.conv4.append(nn.Conv2d(512, 1024, kernel_size=3, padding=1))
        
        self.conv5 = nn.ModuleList([nn.Conv2d(1024, 512, kernel_size=1, padding=0),
                                   nn.Conv2d(512, 1024, kernel_size=3, padding=1),
                                   nn.Conv2d(1024, 512, kernel_size=1, padding=0),
                                   nn.Conv2d(512, 1024, kernel_size=3, padding=1),
                                   nn.Conv2d(1024, 1024, kernel_size=3, padding=1),
                                   nn.Conv2d(1024, 1024, kernel_size=3, stride=2, padding=1)])
        
        self.conv6 = nn.ModuleList([nn.Conv2d(1024, 1024, kernel_size=3, padding=1),
                                  nn.Conv2d(1024, 1024, kernel_size=3, padding=1)])
        
        # Fully-connected layers
        self.fc1 = nn.Linear(7 * 7 * 1024, 2048)  
        self.fc2 = nn.Linear(2048, 7 * 7 * 5)

    def forward(self, x):
        # Conv-block 1
        x = F.max_pool2d(F.relu(self.conv1(x)), (2, 2))
        
        # Conv-block 2
        x = F.max_pool2d(F.relu(self.conv2(x)), (2, 2))
        
        # Conv-block 3
        for conv in self.conv3:
            x = F.relu(conv(x))
        x = F.max_pool2d(x, (2, 2))
        
        # Conv-block 4
        for conv in self.conv4:
            x = F.relu(conv(x))
        x = F.max_pool2d(x, (2, 2))
        
        # Conv-block 5
        for conv in self.conv5:
            x = F.relu(conv(x))
            
        # Conv-block 6
        for conv in self.conv6:
            x = F.relu(conv(x))
        
        # Fully-connected layers
        x = x.view(-1, self.num_flat_features(x))
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        x = x.view(-1, 7, 7, 5)
        
        return x

    def num_flat_features(self, x):
        size = x.size()[1:]  # all dimensions except the batch dimension
        num_features = 1
        for s in size:
            num_features *= s
        return num_features