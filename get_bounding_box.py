import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd
from PIL import Image

def get_bounding_box(y):
    size = np.shape(y)
    yd = np.load('test.npy')
    bbx = np.zeros((2,4))
    idx = np.zeros((2,2))
    n = 0
    for i in range(int(size[1])):
        for j in range(int(size[2])):
            if yd[0,i,j] >0.8:
                bbx[n,:] = np.array(yd[1:5,i,j].flatten())
                idx[n,:] = np.array([i,j])
                n = n + 1

    return bbx, idx