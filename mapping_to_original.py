import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd
from PIL import Image

def mapping_to_original(bbx, idx, path='./test/'):
    data_path = os.path.join(path, 'data/')
    annotation_path = os.path.join(path, 'annotation/')
    file = '1.csv'
    imageName = pd.read_csv(annotation_path +  file).columns.values[0]
    img_path = os.path.join(data_path, imageName+'.jpg')
    I = Image.open(img_path).convert('RGB')
    org_size = I.size
    scale = max(I.size) / 448
    org_bbx = np.round(bbx * scale * 64)
    for i in range(np.shape(org_bbx)[0]):
        org_bbx[i,0] = org_bbx[i,0] + idx[i,0]*64*scale
        org_bbx[i,1] = org_bbx[i,1] + idx[i,1]*64*scale

    return org_bbx.round()