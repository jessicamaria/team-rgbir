# Team-RGBIR Description
This is project "EyeR - A CNN based Near Infrared Style Transfer to
Enhance Pupil Detection in RGB Facial Portraits" developed by Team RGBIR composed of Zi Deng, Jessica Echterhoff, Shawheen Tosifian and Zheng Zhou. This project is for the course ECE 285 at UCSD.
# Requirements
The following packages must be installed to run the demo in this repository:
* tensorflow  
* tensorflow-gpu==1.14.0
* keras
* h5py
* imageio
* matplotlib
* numpy
* opencv-python
* pandas
* scikit-image
* scipy==1.1.0
* pillow 
* scikit-image
* functools
* torch

We recommend installing them using pip using the following code:

```python
pip install --user <package_name>
```

# Code Organization

DataPreparation/ObjectDetection.ipynb --- This file contains code to transform the provided HELEN landmarks to bounding boxes for our object detector. To speed up the initial prototyping we also used a cascade eye classifier to get sample data for out style transfer before the DNN object detector was trained.

PupilDetection/PupilDetection.ipynb --- This file contains code to exemplarily detect a pupil in a given cropped and style transfered eye image.

SRCNN_checkpoint/srcnn_21 --- This directory contains the checkpoint and saved weights for our SRCNN model.

SRCNN_result --- This directory contains the output results of our SRCNN model.

SRCNN_test/Set5 --- This directory contains the input test images of our SRCNN model.

test --- This directory contains the test data images for our CNN-based Object Detection. It is split up into data, label, and annotation.

train --- This directory contains the train data images for our CNN-based Object Detection. It is split up into data, label, and annotation.

Demo1 --- CNN-based Eye Detection.ipynb --- Run this notebook to do CNN-based Object Detection on images from the Helen dataset.

Demo2 --- SRCNN.ipynb --- Run this notebook to do Super resolution (based on SRCNN) on the cropped eye images.

Demo3 --- EyeR_StyleTransfer.ipynb --- Run this notebook to do Style Transfer on the super resolutioned cropped eye images.

bbx.npy --- The file contains information about the bounding box for our object detector.

core.1262 --- This file contains the training weights our Object Detection CNN model.

get_bounding_box.py --- This file is run during our Object Detection CNN model to find the bounding boxes eyes from the Helen dataset. 

Img_010_L_1.jpg --- This image is the style image used for Style Transfer in our model. It is an NIR pupil image.

IoU.py --- This script calculates the Intersection over Union from our bounding box versus the ground truth bounding box we labeled.

label_bounding_box.py --- This script labels the bounding boxes in our Object Detection CNN model.

mapping_to_original.py --- This script maps to the original image in our Object Dectection CNN model.

nntools.py --- This script contains useful functions we need to use for our nn model instatiation and evaluation.

plot_bbx --- This script plots the bounding box that we found.

pre_processing.py --- This script does some pre_processing on the data in our Object Detection CNN model.

test.npy --- This file contains weight values needed for our Object Detection CNN model.

prepare_SRCNN_test.py --- Run this script to prepare the test data for our SRCNN model.

prepare_SRCNN_train.py --- Run this script to prepare the training data for our SRCNN model.

README.md --- This is our README

SRCNN_net.py --- This contains all the set up and model generation code for our SRCNN model. To do training on SRCNN, uncomment the code at the bottom of script and run this script.

use_SRCNN.py --- Run this file do an instance of super resolution for a given input image. The input path must be manually changed within the script.



# Demo Usage

Our project demonstration is split into three ipython notebooks that should be run sequentially starting from "Demo1 - CNN-based Eye Detection.ipynb"

1. Make sure all the proper python packages are installed with their proper versions.
2. Open "Demo1 - CNN-based Eye Detection.ipynb" and run all cells. 
3. Open "Demo2 - SRCNN.ipynb" and run all cells
4. Open "Demo3 - EyeR_StyleTransfer.ipynb" and run all cells.

The output image after eye detection can be found in "SRCNN_test/Set5".

The output image after super resolution can be found in "SRCNN_result". 

The final style transfered image can be viewed in the Demo3 ipython notebook.

# References
SRCNN - https://github.com/Edwardlzy/SRCNN

Style Transfer - https://medium.com/tensorflow/neural-style-transfer-creating-art-with-deep-learning-using-tf-keras-and-eager-execution-7d541ac31398